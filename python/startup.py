import atexit
import os
import readline


def write_history(histfile):
    try:
        readline.write_history_file(histfile)
    except OSError:
        pass


cache_dir = os.environ.get("XDG_CACHE_HOME")
if cache_dir is None:
    home = os.environ["HOME"]
    cache_dir = home + "/.cache"

if not os.path.isdir(cache_dir):
    os.mkdir(cache_dir)

cache_dir = cache_dir + "/python"

if not os.path.isdir(cache_dir):
    os.mkdir(cache_dir)

histfile = cache_dir + "/history"
try:
    readline.read_history_file(histfile)
except OSError:
    pass

atexit.register(write_history, histfile)
