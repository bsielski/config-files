: "${XDG_CONFIG_HOME:=$HOME/.config}"
: "${XDG_DATA_HOME:=$HOME/.local/share}"
: "${XDG_DATA_DIRS:=/usr/local/share:/usr/share}"

mkdir -p "$XDG_DATA_HOME/zsh"

fpath=("$XDG_DATA_HOME/zsh/site-functions" $fpath)

autoload -U colors && colors

stty -ixon
if [[ -e ${XDG_CONFIG_HOME}/ls/colors ]]; then
	eval "$(dircolors -b ${XDG_CONFIG_HOME}/ls/colors)"
else
	eval "$(dircolors -b)"
fi

mkdir -p ${XDG_RUNTIME_DIR}/zsh
autoload -U compinit && compinit -d ${XDG_RUNTIME_DIR}/zsh/compdump
_comp_options+=(globdots)

autoload edit-command-line && \
	zle -N edit-command-line && \
	bindkey -M vicmd v edit-command-line
export KEYTIMEOUT=1

bindkey "^[[3~"  delete-char
bindkey "^[3;5~" delete-char

function fancy-ctrl-z {
	if [[ $#BUFFER -eq 0 ]]; then
		BUFFER="fg"
		zle accept-line
	fi
}
zle -N fancy-ctrl-z
bindkey "^Z" fancy-ctrl-z

vim_mode=''
function set-vim-mode {
	case $KEYMAP in
		vicmd) vim_mode="─[CMD]";;
		viins|main) vim_mode='';;
	esac
	zle reset-prompt
}

# Fix <http://zsh.sourceforge.net/FAQ/zshfaq03.html#l25> in st
if [[ -n ${terminfo[smkx]} ]] && [[ -n ${terminfo[rmkx]} ]]; then
	function zle-line-init () { echoti smkx; set-vim-mode }
	function zle-line-finish () { echoti rmkx; set-vim-mode }
	zle -N zle-line-init
	zle -N zle-line-finish
else
	function zle-line-init () { set-vim-mode }
	function zle-line-finish () { set-vim-mode }
	zle -N zle-line-init
	zle -N zle-line-finish
fi

if [[ -d ${XDG_CONFIG_HOME}/shell.d ]]; then
	for file in ${XDG_CONFIG_HOME}/shell.d/*; do
		source $file
	done
fi

HISTSIZE=100000
SAVEHIST=$HISTSIZE
HISTFILE="$XDG_DATA_HOME/zsh/history"
setopt hist_ignore_all_dups hist_ignore_space hist_fcntl_lock hist_reduce_blanks

function zle-keymap-select { set-vim-mode }
zle -N zle-keymap-select

if command -v fzf >/dev/null; then
__fzfcmd() {
	[ -n "$TMUX_PANE" ] && { [ "${FZF_TMUX:-0}" != 0 ] || [ -n "$FZF_TMUX_OPTS" ]; } &&
		echo "fzf-tmux ${FZF_TMUX_OPTS:--d${FZF_TMUX_HEIGHT:-40%}} -- " || echo "fzf"
}
fzf-history-widget() {
	setopt localoptions noglobsubst noposixbuiltins pipefail 2> /dev/null
	local selected=$(fc -rln 1 | FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} $FZF_DEFAULT_OPTS --tiebreak=index --bind=ctrl-r:toggle-sort $FZF_CTRL_R_OPTS --query=${(qqq)LBUFFER} --no-multi" $(__fzfcmd))
	local ret=$?
	if [ -n "$selected" ]; then
		BUFFER="$selected"
	fi
	zle reset-prompt
	zle end-of-line
	return $ret
}
zle     -N   fzf-history-widget
bindkey '^R' fzf-history-widget
#source /usr/share/fzf/completion.zsh
else
bindkey '^R' history-incremental-search-backward
fi

HISTORY_IGNORE="(forget-cmd|forget-cmd *|man *|mpv *|yt-dlp *|youtube-dl *)"
forget-cmd() {
	is_int() {
		return $(test "$@" -eq "$@" > /dev/null 2>&1)
	}

	local which_cmd="-1"
	if [ $# -ne 0 ]; then
		if $(is_int "${1}"); then
			which_cmd="-$1"
		else
			echo "Unknown argument passed. Exiting..."
			return 1
		fi
	fi

	HISTORY_IGNORE="${HISTORY_IGNORE:0:-1}|$(fc -ln "$which_cmd" "$which_cmd"))"
}

ZSH_GIT_PROMPT=''
while read -d ':' dir; do
	if [[ -f "$dir/zsh-git-prompt/include.sh" ]]; then
		source "$dir/zsh-git-prompt/include.sh"
		ZSH_GIT_PROMPT='git_status'
		break
	fi
done <<< "$XDG_DATA_HOME:$XDG_DATA_DIRS:"

git_status=''
function precmd {
	if [[ -n "$ZSH_GIT_PROMPT" ]]; then
		git_status="$($ZSH_GIT_PROMPT)"
	fi
}

ZSH_THEME_GIT_PROMPT_PREFIX="─["
ZSH_THEME_GIT_PROMPT_SUFFIX="]";
ZSH_THEME_GIT_PROMPT_HASH_PREFIX=":"
ZSH_THEME_GIT_PROMPT_SEPARATOR="|"
ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg_bold[white]%}"
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg_bold[green]%}%{+%G%}"
ZSH_THEME_GIT_PROMPT_CONFLICTS="%{$fg_bold[magenta]%}%{!%G%}"
ZSH_THEME_GIT_PROMPT_CHANGED="%{$fg_bold[red]%}%{×%G%}"
ZSH_THEME_GIT_PROMPT_BEHIND="%{↓%G%}"
ZSH_THEME_GIT_PROMPT_AHEAD="%{↑%G%}"
ZSH_THEME_GIT_PROMPT_STASH="%{$fg[white]%}%{ 📦%G%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{…%G%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}%{✔%G%}"
ZSH_THEME_GIT_PROMPT_LOCAL=""
ZSH_THEME_GIT_PROMPT_UPSTREAM_FRONT="{%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_UPSTREAM_END="%{$reset_color%}}"
ZSH_THEME_GIT_PROMPT_MERGING="%{$fg_bold[magenta]%}MERGING%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_REBASE="%{$fg_bold[magenta]%}REBASE%{$reset_color%} "

setopt prompt_subst

PROMPT=$'┌──[%{$fg_bold[green]%}%n@%M%{$reset_color%}]'
PROMPT+='─[%{$fg_bold[blue]%}%~%{$reset_color%}]'
PROMPT+='%(1j.─[%{$fg_bold[yellow]%}bg%{$reset_color%}].)'
PROMPT+='%(0?..%(148?..─[%{$fg_bold[red]%}%?%{$reset_color%}]))'
PROMPT+='${vim_mode}'
PROMPT+='${git_status}'
PROMPT+='
└───╼ %{$fg_bold[blue]%}#%{$reset_color%} '

RPROMPT=
