#!/bin/sh
if [ "$IFNAME" != ppp-luminex ]; then
	exit 0
fi
ip route add 172.16.0.0/16 dev "$IFNAME" scope link
