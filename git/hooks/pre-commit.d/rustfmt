#!/usr/bin/env python3
from functools import reduce
import re
import subprocess
import sys
import tempfile

def staged_files_formatted():
    try:
        output = subprocess.check_output([
            'git', 'diff-index', '--cached', '--diff-filter=AM', '--no-renames', 'HEAD',
        ]).decode('utf-8')
    except subprocess.CalledProcessError as err:
        print(err)
        return 2
    except UnicodeError as err:
        print(err)
        return 2

    mapped = map(lambda line: entry_formatted(parse_diff(line)), output.splitlines())

    if not reduce(lambda a, b: a and b, mapped, True):
        return 1
    return 0


diff_pattern = re.compile(r'^:([0-7]{6}) ([0-7]{6}) ([a-f0-9]{40}) ([a-f0-9]{40}) ([A-Z])(\d+)?\t([^\t]+)(?:\t([^\t]+))?$')

def parse_diff(line):
    match = diff_pattern.match(line)
    if not match:
        raise ValueError(f'Failed to parse diff-index line: {line}')
    return {
        'src_mode': unless_zeroed(match.group(1)),
        'dst_mode': unless_zeroed(match.group(2)),
        'src_hash': unless_zeroed(match.group(3)),
        'dst_hash': unless_zeroed(match.group(4)),
        'status': match.group(5),
        'score': int(match.group(6)) if match.group(6) else None,
        'src_path': match.group(7),
        'dst_path': match.group(8)
    }


zero_pattern = re.compile('^0+$')

def unless_zeroed(string):
    return string if not zero_pattern.match(string) else None


rust_file_pattern = re.compile(r'^.*\.rs$')
rustfmt_fixup_pattern = re.compile('^Diff in /dev/stdin')

def entry_formatted(entry):
    if not rust_file_pattern.match(entry['src_path']):
        return True

    with open(f'{tmp_dir}/rustfmt.toml', 'w+') as rustfmt_config_file:
        file_content = subprocess.Popen(
            ['git', 'cat-file', '-p', entry['dst_hash']],
            stdout=subprocess.PIPE,
        )

        rustfmt_config = subprocess.Popen(
            ['rustfmt', '--print-config', 'current', entry['src_path']],
            stdout=rustfmt_config_file,
            stderr=subprocess.PIPE,
        )

        check_fmt = subprocess.Popen(
            ['rustfmt', '--check', '/dev/stdin', '--config-path', str(tmp_dir)],
            stdin=file_content.stdout,
            stdout=subprocess.PIPE,
        )

        if file_content.wait() != 0:
            raise ValueError(f'Unable to read file content from object database: {entry["dst_hash"]}')

        if rustfmt_config.wait() != 0:
            stderr = rustfmt_config.stderr.read().decode('utf-8')
            raise ValueError(f'Unable to write rustfmt config to file: {stderr}')

        if check_fmt.wait() != 0:
            for line in check_fmt.stdout.read().decode('utf-8').splitlines():
                print(rustfmt_fixup_pattern.sub(f'Diff in {entry["src_path"]}', line))
            return False
        return True

if __name__ == '__main__':
    with tempfile.TemporaryDirectory() as tmp_dir:
        sys.exit(staged_files_formatted())
