#!/bin/sh -eu
bare_repo="$(git config --bool core.bare)"
if [ "$bare_repo" = "false" ]; then
	export GIT_TOP_LEVEL="$(git rev-parse --show-toplevel)"
fi

readonly hook_dir="$0.d"
[ ! -d "$hook_dir" ] && exit 0

readonly tmpdir="$(mktemp -d)"
finish() {
	rm -rf "$tmpdir"
}
trap finish EXIT

readonly hook_type=$(basename "$0")
for hook in "$hook_dir"/*; do
	[ -f "$hook" ] && [ -x "$hook" ] || continue

	hook_name="$(basename "$hook")"
	export GIT_HOOK_CONFIG="hooks.$hook_type.$hook_name"
	enabled="$(git config --bool --default=false "$GIT_HOOK_CONFIG.enable")"
	[ "$enabled" = "true" ] || continue

	printf "\033[1;33mRunning %s hook: '%s'\033[0m\n" "$hook_type" "$hook_name"
	if [ ! -f "$tmpdir/stdin" ]; then
		tee "$tmpdir/stdin" < /dev/stdin | "$hook" "$@"
	else
		"$hook" "$@" < "$tmpdir/stdin"
	fi
done
