local cmd = vim.cmd
local keymap = vim.keymap
local opt = vim.opt

-- Enable mouse in all modes
opt.mouse = 'a'

-- Don't show the mode in the command bar
opt.showmode = false

-- Required for operations modifying multiple buffers
opt.hidden = true

-- Disable intro message
opt.shortmess:append('I')

-- Disable automatic text wrapping to textwidth
opt.formatoptions:remove('t')

-- Show non-text characters
opt.list = true
opt.listchars = {
    trail = '¤',
    tab = '▷ ',
    nbsp = '█',
    extends = '»',
    precedes = '«',
}
opt.fillchars = { eob = ' ' }

-- Open new windows below and to the right of existing ones
opt.splitbelow = true
opt.splitright = true

-- Search options
opt.ignorecase = true
opt.smartcase = true

-- Persistent undo across exits
opt.undofile = true

-- Enable line numbers
opt.number = true
opt.relativenumber = true

-- Use only 1 status bar
opt.laststatus = 3

opt.scrolloff = 5

opt.signcolumn = 'number'

opt.colorcolumn = '+1'

-- Enable spelling and set the desired language
opt.spell = true
opt.spelllang = { 'en_us', 'nl' }

-- Key bindings
vim.g.mapleader = ' '
keymap.set({'n', 'v'}, ' ', '<Nop>')
--vim.g.maplocalleader = ','

-- Remap ; to : in normal and visual mode (who has time to press shift anyway)
keymap.set({'n', 'v'}, ';', ':')

-- Disable F1 opening help
keymap.set('', '<F1>', '<Nop>')

-- Disable Q opening ex mode
keymap.set('n', 'Q', '<Nop>')

-- Copy until the end of the line with Y
keymap.set('n', 'Y', 'y$')

-- Easier movement to start/end of lines
keymap.set({'n', 'v'}, 'H', '^')
keymap.set({'n', 'v'}, 'L', 'g_')

-- Easier split movements
keymap.set({'n', 't'}, '<c-h>',   '<cmd>wincmd h<cr>', { silent = true })
--keymap.set({'n', 't'}, '<c-s-h>', '<cmd>wincmd H<cr>', { silent = true })
keymap.set({'n', 't'}, '<c-j>',   '<cmd>wincmd j<cr>', { silent = true })
--keymap.set({'n', 't'}, '<c-s-j>', '<cmd>wincmd J<cr>', { silent = true })
keymap.set({'n', 't'}, '<c-k>',   '<cmd>wincmd k<cr>', { silent = true })
--keymap.set({'n', 't'}, '<c-s-k>', '<cmd>wincmd K<cr>', { silent = true })
keymap.set({'n', 't'}, '<c-l>',   '<cmd>wincmd l<cr>', { silent = true })
--keymap.set({'n', 't'}, '<c-s-l>', '<cmd>wincmd L<cr>', { silent = true })

-- Easier split resizing
keymap.set({'n', 't'}, '<c-up>',    '<cmd>wincmd +<cr>', { silent = true })
keymap.set({'n', 't'}, '<c-down>',  '<cmd>wincmd -<cr>', { silent = true })
keymap.set({'n', 't'}, '<c-left>',  '<cmd>wincmd <<cr>', { silent = true })
keymap.set({'n', 't'}, '<c-right>', '<cmd>wincmd ><cr>', { silent = true })

-- Use tab to indent in visual mode
keymap.set('v', '<Tab>',   '>gv')
keymap.set('v', '<S-Tab>', '<gv')

cmd.colorscheme('base16-default-dark')

require("plugins")

-- LSP Diagnostics Options Setup
local sign = function(opts)
    vim.fn.sign_define(opts.name, {
        texthl = opts.name,
        text = opts.text,
        numhl = ''
    })
end

sign({name = 'DiagnosticSignError', text = ''})
sign({name = 'DiagnosticSignWarn', text = ''})
sign({name = 'DiagnosticSignHint', text = '󰠠'})
sign({name = 'DiagnosticSignInfo', text = ''})

vim.diagnostic.config({
    virtual_text = false,
    signs = true,
    update_in_insert = true,
    underline = true,
    severity_sort = false,
    float = {
        border = 'rounded',
        source = 'always',
        header = '',
        prefix = '',
    },
})

local editorconfig = require('editorconfig')
editorconfig.properties.vim_filetype = function(bufnr, val)
    vim.bo[bufnr].filetype = val
end
