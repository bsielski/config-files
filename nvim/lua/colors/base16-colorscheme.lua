local M = {}
local hex_re = vim.regex('#\\x\\x\\x\\x\\x\\x')

local HEX_DIGITS = {
    ['0'] = 0,
    ['1'] = 1,
    ['2'] = 2,
    ['3'] = 3,
    ['4'] = 4,
    ['5'] = 5,
    ['6'] = 6,
    ['7'] = 7,
    ['8'] = 8,
    ['9'] = 9,
    ['a'] = 10,
    ['b'] = 11,
    ['c'] = 12,
    ['d'] = 13,
    ['e'] = 14,
    ['f'] = 15,
    ['A'] = 10,
    ['B'] = 11,
    ['C'] = 12,
    ['D'] = 13,
    ['E'] = 14,
    ['F'] = 15,
}

local function hex_to_rgb(hex)
    return HEX_DIGITS[string.sub(hex, 1, 1)] * 16 +  HEX_DIGITS[string.sub(hex, 2, 2)],
        HEX_DIGITS[string.sub(hex, 3, 3)] * 16 +  HEX_DIGITS[string.sub(hex, 4, 4)],
        HEX_DIGITS[string.sub(hex, 5, 5)] * 16 +  HEX_DIGITS[string.sub(hex, 6, 6)]
end

local function rgb_to_hex(r, g, b)
  return bit.tohex(bit.bor(bit.lshift(r, 16), bit.lshift(g, 8), b), 6)
end

local function darken(hex, pct)
    pct = 1 - pct
    local r, g, b = hex_to_rgb(string.sub(hex, 2))
    r = math.floor(r * pct)
    g = math.floor(g * pct)
    b = math.floor(b * pct)
    return string.format("#%s", rgb_to_hex(r, g, b))
end

local all_decorations = {
    'bold', 'italic', 'reverse', 'standout',
    'underline', 'underlineline', 'undercurl',
    'underdot', 'underdash', 'strikethrough',
}

M.highlight = setmetatable({}, {
    __newindex = function(_, hlgroup, args)
        if (type(args) == 'string') then
            args = { link = args }
        else
            local contains_decorations = false
            for _, decoration in pairs(all_decorations) do
                if args[decoration] ~= nil then
                    contains_decorations = true
                end
            end
            if (contains_decorations and args.nocombine) or args.decorations == 'none' then
                args.decorations = nil
                for _, decoration in pairs(all_decorations) do
                    if args[decoration] ~= nil then
                        args[decoration] = false
                    end
                end
            end
        end
        vim.api.nvim_set_hl(0, hlgroup, args)
    end
})

--- Creates a base16 colorscheme using the colors specified.
--
-- The default Vim highlight groups (including User[1-9]), highlight groups
-- pertaining to Neovim's builtin LSP, and highlight groups pertaining to
-- Treesitter will be defined.
--
-- It's worth noting that many colorschemes will specify language specific
-- highlight groups like rubyConstant or pythonInclude. However, I don't do
-- that here since these should instead be linked to an existing highlight
-- group.
--
-- @param colors (table) table with keys 'base00', 'base01', 'base02',
--   'base03', 'base04', 'base05', 'base06', 'base07', 'base08', 'base09',
--   'base0A', 'base0B', 'base0C', 'base0D', 'base0E', 'base0F'. Each key should
--   map to a valid 6 digit hex color. If a string is provided, the
--   corresponding table specifying the colorscheme will be used.
function M.setup(colors)
    if vim.fn.exists('syntax_on') then
        vim.cmd('syntax reset')
    end
    vim.opt.termguicolors = true

    local hi = M.highlight

    -- Vim editor colors
    hi.Normal       = { fg = colors.base05, bg = colors.base00 }
    hi.Bold         = { bold = true }
    hi.Debug        = { fg = colors.base08 }
    hi.Directory    = { fg = colors.base0D }
    hi.Error        = { fg = colors.base00, bg = colors.base08 }
    hi.ErrorMsg     = { fg = colors.base08, bg = colors.base00 }
    hi.Exception    = { fg = colors.base08 }
    hi.FoldColumn   = { fg = colors.base0C, bg = colors.base00 }
    hi.Folded       = { fg = colors.base03, bg = colors.base01 }
    hi.IncSearch    = { fg = colors.base01, bg = colors.base09, decorations = 'none' }
    hi.Italic       = { italic = true }
    hi.Macro        = { fg = colors.base08 }
    hi.MatchParen   = { fg = nil,           bg = colors.base03 }
    hi.ModeMsg      = { fg = colors.base0B }
    hi.MoreMsg      = { fg = colors.base0B }
    hi.Question     = { fg = colors.base0D }
    hi.Search       = { fg = colors.base01, bg = colors.base0A }
    hi.Substitute   = { fg = colors.base01, bg = colors.base0A, decorations = 'none' }
    hi.SpecialKey   = { fg = colors.base03 }
    hi.TooLong      = { fg = colors.base08 }
    hi.Underlined   = { underline = true }
    hi.Visual       = { fg = nil,           bg = colors.base02 }
    hi.VisualNOS    = { fg = colors.base08 }
    hi.WarningMsg   = { fg = colors.base08 }
    hi.WildMenu     = { fg = colors.base08, bg = colors.base0A }
    hi.Title        = { fg = colors.base0D, bg = nil,           decorations = 'none' }
    hi.Conceal      = { fg = colors.base0D, bg = colors.base00 }
    hi.Cursor       = { fg = colors.base00, bg = colors.base05 }
    hi.NonText      = { fg = colors.base03 }
    hi.LineNr       = { fg = colors.base04, bg = colors.base00 }
    hi.SignColumn   = { fg = colors.base04, bg = colors.base00 }
    hi.StatusLine   = { fg = colors.base05, bg = colors.base02, decorations = 'none' }
    hi.StatusLineNC = { fg = colors.base04, bg = colors.base01, decorations = 'none' }
    hi.VertSplit    = { fg = colors.base05, bg = colors.base00, decorations = 'none' }
    hi.ColorColumn  = { fg = nil,           bg = colors.base01, decorations = 'none' }
    hi.CursorColumn = { fg = nil,           bg = colors.base01, decorations = 'none' }
    hi.CursorLine   = { fg = nil,           bg = colors.base01, decorations = 'none' }
    hi.CursorLineNr = { fg = colors.base04, bg = colors.base01 }
    hi.QuickFixLine = { fg = nil,           bg = colors.base01, decorations = 'none' }
    hi.PMenu        = { fg = colors.base05, bg = colors.base01, decorations = 'none' }
    hi.PMenuSel     = { fg = colors.base01, bg = colors.base05 }
    hi.TabLine      = { fg = colors.base03, bg = colors.base01, decorations = 'none' }
    hi.TabLineFill  = { fg = colors.base03, bg = colors.base01, decorations = 'none' }
    hi.TabLineSel   = { fg = colors.base0B, bg = colors.base01, decorations = 'none' }

    -- Standard syntax highlighting
    hi.Boolean      = { fg = colors.base09 }
    hi.Character    = { fg = colors.base08 }
    hi.Comment      = { fg = colors.base03 }
    hi.Conditional  = { fg = colors.base0E }
    hi.Constant     = { fg = colors.base05 }
    hi.Define       = { fg = colors.base0E, decorations = 'none' }
    hi.Delimiter    = { fg = colors.base0F }
    hi.Float        = 'Number'
    hi.Function     = { fg = colors.base0D }
    hi.Identifier   = { fg = colors.base05 }
    hi.Include      = { fg = colors.base0E }
    hi.Keyword      = { fg = colors.base0E }
    hi.Label        = 'Conditional'
    hi.Number       = { fg = colors.base09 }
    hi.Operator     = { fg = colors.base05, decorations = 'none' }
    hi.PreProc      = { fg = colors.base0E }
    hi.Repeat       = 'Conditional'
    hi.Special      = { fg = colors.base0C }
    hi.SpecialChar  = { fg = colors.base0F }
    hi.Statement    = { fg = colors.base08 }
    hi.StorageClass = 'Structure'
    hi.String       = { fg = colors.base0B }
    hi.Structure    = { fg = colors.base0E }
    hi.Tag          = { fg = colors.base0A }
    hi.Todo         = { fg = colors.base0A, bg = colors.base01 }
    hi.Type         = { fg = colors.base0A, decorations = 'none' }
    hi.Typedef      = { fg = colors.base0A }

    -- Diff highlighting
    hi.DiffAdd     = { fg = colors.base0B, bg = colors.base00 }
    hi.DiffChange  = { fg = colors.base03, bg = colors.base00 }
    hi.DiffDelete  = { fg = colors.base08, bg = colors.base00 }
    hi.DiffText    = { fg = colors.base0D, bg = colors.base00 }
    hi.DiffAdded   = { fg = colors.base0B, bg = colors.base00 }
    hi.DiffFile    = { fg = colors.base08, bg = colors.base00 }
    hi.DiffNewFile = { fg = colors.base0B, bg = colors.base00 }
    hi.DiffLine    = { fg = colors.base0D, bg = colors.base00 }
    hi.DiffRemoved = { fg = colors.base08, bg = colors.base00 }

    -- Git highlighting
    hi.gitcommitOverflow      = { fg = colors.base08 }
    hi.gitcommitSummary       = { fg = colors.base0B }
    hi.gitcommitComment       = { fg = colors.base03 }
    hi.gitcommitUntracked     = { fg = colors.base03 }
    hi.gitcommitDiscarded     = { fg = colors.base03 }
    hi.gitcommitSelected      = { fg = colors.base03 }
    hi.gitcommitHeader        = { fg = colors.base0E }
    hi.gitcommitSelectedType  = { fg = colors.base0D }
    hi.gitcommitUnmergedType  = { fg = colors.base0D }
    hi.gitcommitDiscardedType = { fg = colors.base0D }
    hi.gitcommitBranch        = { fg = colors.base09, bold = true }
    hi.gitcommitUntrackedFile = { fg = colors.base0A }
    hi.gitcommitUnmergedFile  = { fg = colors.base08, bold = true }
    hi.gitcommitDiscardedFile = { fg = colors.base08, bold = true }
    hi.gitcommitSelectedFile  = { fg = colors.base0B, bold = true }

    -- GitGutter highlighting
    hi.GitGutterAdd          = { fg = colors.base0B, bg = colors.base00 }
    hi.GitGutterChange       = { fg = colors.base0D, bg = colors.base00 }
    hi.GitGutterDelete       = { fg = colors.base08, bg = colors.base00 }
    hi.GitGutterChangeDelete = { fg = colors.base0E, bg = colors.base00 }

    -- Spelling highlighting
    hi.SpellBad   = { undercurl = true, sp = colors.base08 }
    hi.SpellLocal = { undercurl = true, sp = colors.base0C }
    hi.SpellCap   = { undercurl = true, sp = colors.base0D }
    hi.SpellRare  = { undercurl = true, sp = colors.base0E }

    hi.DiagnosticError                    = { fg = colors.base08, decorations = 'none' }
    hi.DiagnosticWarn                     = { fg = colors.base0E, decorations = 'none' }
    hi.DiagnosticInfo                     = { fg = colors.base05, decorations = 'none' }
    hi.DiagnosticHint                     = { fg = colors.base0C, decorations = 'none' }
    hi.DiagnosticUnnecessary              = {}
    hi.DiagnosticUnderlineError           = { undercurl = true, sp = colors.base08 }
    hi.DiagnosticUnderlineWarning         = { undercurl = true, sp = colors.base0E }
    hi.DiagnosticUnderlineWarn            = { undercurl = true, sp = colors.base0E }
    hi.DiagnosticUnderlineInformation     = { undercurl = true, sp = colors.base0F }
    hi.DiagnosticUnderlineHint            = { undercurl = true, sp = colors.base0C }

    hi.LspReferenceText                   = { underline = true, sp = colors.base04 }
    hi.LspReferenceRead                   = { underline = true, sp = colors.base04 }
    hi.LspReferenceWrite                  = { underline = true, sp = colors.base04 }
    hi.LspDiagnosticsDefaultError         = 'DiagnosticError'
    hi.LspDiagnosticsDefaultWarning       = 'DiagnosticWarn'
    hi.LspDiagnosticsDefaultInformation   = 'DiagnosticInfo'
    hi.LspDiagnosticsDefaultHint          = 'DiagnosticHint'
    hi.LspDiagnosticsUnderlineError       = 'DiagnosticUnderlineError'
    hi.LspDiagnosticsUnderlineWarning     = 'DiagnosticUnderlineWarning'
    hi.LspDiagnosticsUnderlineInformation = 'DiagnosticUnderlineInformation'
    hi.LspDiagnosticsUnderlineHint        = 'DiagnosticUnderlineHint'

    hi['@lsp.type.class']                       = '@type'
    hi['@lsp.type.comment']                     = '@comment'
    hi['@lsp.type.decorator']                   = '@macro'
    hi['@lsp.type.enum']                        = '@type'
    hi['@lsp.type.enumMember']                  = '@constant'
    hi['@lsp.type.interface']                   = '@interface'
    hi['@lsp.type.keyword']                     = '@keyword'
    hi['@lsp.type.namespace']                   = '@namespace'
    hi['@lsp.type.parameter']                   = '@parameter'
    hi['@lsp.type.property']                    = '@property'
    hi['@lsp.type.struct']                      = '@type'
    hi['@lsp.type.variable']                    = '@variable'

    hi['@lsp.typemod.function.defaultLibrary']  = '@function.builtin'
    hi['@lsp.typemod.method.defaultLibrary']    = '@function.builtin'
    hi['@lsp.typemod.operator.injected']        = '@operator'
    hi['@lsp.typemod.string.injected']          = '@string'
    hi['@lsp.typemod.variable.defaultLibrary']  = '@variable.builtin'
    hi['@lsp.typemod.variable.injected']        = '@variable'

    hi['@lsp.typemod.derive.attribute.rust']    = '@interface'
    hi['@lsp.typemod.keyword.public.rust']      = '@namespace'

    hi['@boolean']          = 'Boolean'
    hi['@character']        = 'Character'
    hi['@comment']          = 'Comment'
    hi['@conditional']      = 'Conditional'
    hi['@constant']         = 'Constant'
    hi['@field']            = 'Identifier'
    hi['@float']            = 'Float'
    hi['@function']         = 'Function'
    hi['@function.builtin'] = 'Function'
    hi['@function.marco']   = 'Macro'
    hi['@include']          = 'Include'
    hi['@interface']        = { fg = colors.base0A, italic = true }
    hi['@keyword']          = 'Keyword'
    hi['@method']           = 'Function'
    hi['@namespace']        = { fg = colors.base0C, decorations = 'none' }
    hi['@number']           = 'Number'
    hi['@operator']         = 'Operator'
    hi['@repeat']           = 'Repeat'
    hi['@text']             = { fg = colors.base05, decorations = 'none' }
    hi['@type']             = 'Type'
    hi['@type.builtin']     = 'Type'
    hi['@type.qualifier']   = 'StorageClass'
    hi['@variable']         = 'Identifier'
    hi['@label']            = 'Label'

    hi['@string']           = 'String'
    hi['@string.regex']     = { fg = colors.base0C, decorations = 'none' }
    hi['@string.escape']    = 'Special'
    hi['@string.special']   = 'Special'

    hi.NvimInternalError = { fg = colors.base00, bg = colors.base08, decorations = 'none' }

    hi.NormalFloat  = { fg = colors.base05, bg = colors.base00 }
    hi.FloatBorder  = { fg = colors.base05, bg = colors.base00 }
    hi.NormalNC     = { fg = colors.base05, bg = colors.base00 }
    hi.TermCursor   = { fg = colors.base00, bg = colors.base05, decorations = 'none' }
    hi.TermCursorNC = { fg = colors.base00, bg = colors.base05 }

    hi.User1 = { fg = colors.base08, bg = colors.base02, decorations = 'none' }
    hi.User2 = { fg = colors.base0E, bg = colors.base02, decorations = 'none' }
    hi.User3 = { fg = colors.base05, bg = colors.base02, decorations = 'none' }
    hi.User4 = { fg = colors.base0C, bg = colors.base02, decorations = 'none' }
    hi.User5 = { fg = colors.base01, bg = colors.base02, decorations = 'none' }
    hi.User6 = { fg = colors.base05, bg = colors.base02, decorations = 'none' }
    hi.User7 = { fg = colors.base05, bg = colors.base02, decorations = 'none' }
    hi.User8 = { fg = colors.base00, bg = colors.base02, decorations = 'none' }
    hi.User9 = { fg = colors.base00, bg = colors.base02, decorations = 'none' }

    hi.TreesitterContext = { bg = colors.base01, italic = true }

    vim.g.terminal_color_0  = colors.base00
    vim.g.terminal_color_1  = colors.base08
    vim.g.terminal_color_2  = colors.base0B
    vim.g.terminal_color_3  = colors.base0A
    vim.g.terminal_color_4  = colors.base0D
    vim.g.terminal_color_5  = colors.base0E
    vim.g.terminal_color_6  = colors.base0C
    vim.g.terminal_color_7  = colors.base05
    vim.g.terminal_color_8  = colors.base03
    vim.g.terminal_color_9  = colors.base08
    vim.g.terminal_color_10 = colors.base0B
    vim.g.terminal_color_11 = colors.base0A
    vim.g.terminal_color_12 = colors.base0D
    vim.g.terminal_color_13 = colors.base0E
    vim.g.terminal_color_14 = colors.base0C
    vim.g.terminal_color_15 = colors.base07
end

return M
