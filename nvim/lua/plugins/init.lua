local lazy_path = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazy_path) then
    vim.fn.system({
        "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazy_path,
    })
end
vim.opt.rtp:prepend(lazy_path)

require("lazy").setup({
    "cespare/vim-toml",
    "LnL7/vim-nix",
    "wgwoods/vim-systemd-syntax",
    "igankevich/mesonic",
    "ledger/vim-ledger",
    "bfrg/vim-cpp-modern",
    "stijngeysen/dm.vim",

    "farmergreg/vim-lastplace",

    { "tpope/vim-eunuch", event = "VeryLazy" },

    { "mbbill/undotree", event = "VeryLazy" },

    { "stevearc/dressing.nvim", event = "VeryLazy" },

    {
        "mhinz/vim-grepper",
        cmd = "Grepper",
        init = function()
            local opts = { silent = true }
            vim.keymap.set("n", "<leader>g", "<cmd>Grepper<cr>", opts)
            vim.keymap.set("n", "<leader>G", "<cmd>Grepper -cword -noprompt<cr>", opts)
            vim.g.grepper = {
                tools = { "rg" },
                simple_prompt = 1,
            }
        end
    },

    { import = "plugins.lazy" },

    {
        "saecki/crates.nvim",
        tag = "stable",
        dependencies = { "nvim-lua/plenary.nvim" },
        event = { "BufRead Cargo.toml" },
        config = function()
            require("crates").setup()
        end,
    }
}, {
    change_detection = {
        notify = false,
    },
})
