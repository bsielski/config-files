local prequire = function(...)
    local status, lib = pcall(require, ...)
    if status then
        return lib
    end
    return nil
end

local local_lspconfig = function(client, file)
    local config = {
        enable = true,
        auto_format = false,
        highlighting = true,
    }

    if client.workspace_folders == nil then
        return config
    end

    for _, workspace_folder in pairs(client.workspace_folders) do
        local dir = workspace_folder.name

        if vim.startswith(file, dir) then
            local config_file = {}
            local f, err = loadfile(dir .. "/.lspconfig.lua", "t", config_file)
            if f ~= nil then
                f()

                if config_file ~= nil then
                    local parsed_config = config_file[client.name]
                    if parsed_config ~= nil then
                        for k, v in pairs(parsed_config) do config[k] = v end
                    end
                end
            end
        end
    end

    return config
end

local lsp_format_filter = function(client)
    local config = local_lspconfig(client, vim.api.nvim_buf_get_name(0))
    return config.auto_format
end

local lsp_format = function()
    vim.lsp.buf.format({
        filter = lsp_format_filter,
    })
end

return {
    "neovim/nvim-lspconfig",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
        "hrsh7th/cmp-nvim-lsp",
    },
    config = function()
        local lspconfig = require("lspconfig")

        local telescope_builtin = prequire("telescope.builtin")

        local implementation = vim.lsp.buf.implementation
        if telescope_builtin then
            implementation = function()
                telescope_builtin.lsp_implementations({
                    initial_mode = "normal",
                })
            end
        end

        local document_highlight = function()
            vim.lsp.buf.clear_references()
            vim.lsp.buf.document_highlight()
        end

        vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float)
        vim.keymap.set("n", "[e",        vim.diagnostic.goto_next)
        vim.keymap.set("n", "]e",        vim.diagnostic.goto_prev)
        vim.keymap.set("n", "<leader>E", vim.diagnostic.setloclist)

        vim.keymap.set("n", "gD",        vim.lsp.buf.declaration)
        vim.keymap.set("n", "gd",        vim.lsp.buf.definition)
        vim.keymap.set("n", "gt",        vim.lsp.buf.type_definition)
        vim.keymap.set("n", "gi",        implementation)
        vim.keymap.set("n", "gr",        vim.lsp.buf.references)
        vim.keymap.set("n", "<leader>u", document_highlight)
        vim.keymap.set("n", "K",         vim.lsp.buf.hover)
        vim.keymap.set("n", "gs",        vim.lsp.buf.signature_help)
        vim.keymap.set("i", "<c-s>",     vim.lsp.buf.signature_help)

        vim.keymap.set("n", "<leader>f", vim.lsp.buf.format)
        vim.keymap.set("n", "<F2>",      vim.lsp.buf.rename)
        vim.keymap.set("n", "<leader>A", vim.lsp.buf.code_action)

        local lspconfig_augroup_id = vim.api.nvim_create_augroup("lspconfig", {})

        vim.api.nvim_create_autocmd("BufWritePre", {
            group = lspconfig_augroup_id,
            callback = lsp_format,
        })

        vim.api.nvim_create_autocmd("LspAttach", {
            group = lspconfig_augroup_id,
            callback = function(args)
                local client = vim.lsp.get_client_by_id(args.data.client_id)

                local config = local_lspconfig(client, vim.api.nvim_buf_get_name(args.buf))

                if not config.enable then
                    print(client.name .. ": disabled")
                    client.stop()
                    return
                end

                if not config.auto_format then
                    print(client.name .. ": automatic code formatting disabled")
                end

                if not config.highlighting then
                    client.server_capabilities.semanticTokensProvider = nil
                end
            end,
        })

        local capabilities =  require("cmp_nvim_lsp").default_capabilities()

        local border = {
            {"╭", "FloatBorder"},
            {"─", "FloatBorder"},
            {"╮", "FloatBorder"},
            {"│", "FloatBorder"},
            {"╯", "FloatBorder"},
            {"─", "FloatBorder"},
            {"╰", "FloatBorder"},
            {"│", "FloatBorder"},
        }

        local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
        vim.lsp.util.open_floating_preview = function(contents, syntax, opts, ...)
            opts = opts or {}
            opts.border = opts.border or border
            return orig_util_open_floating_preview(contents, syntax, opts, ...)
        end

        lspconfig["rust_analyzer"].setup({
            capabilities = capabilities,
            settings = {
                ["rust-analyzer"] = {
                    cargo = {
                        buildScripts = {
                            enable = true,
                        },
                    },
                    procMacro = {
                        enable = true,
                    },
                },
            },
        })

        lspconfig["clangd"].setup({
            capabilities = capabilities,
            cmd = {"clangd", "--background-index", "--fallback-style=none", "--header-insertion=never"},
        })

        lspconfig["pylsp"].setup({
            capabilities = capabilities,
            settings = {
                pylsp = {
                    plugins = {
                        pycodestyle = {
                            maxLineLength = 100
                        },
                        flake8 = {
                            maxLineLength = 100
                        },
                    }
                }
            }
        })

        lspconfig["lua_ls"].setup({
            capabilities = capabilities,
            on_init = function(client)
                local path = client.workspace_folders[1].name
                if not vim.loop.fs_stat(path .. '/.luarc.json') and not vim.loop.fs_stat(path .. '/.luarc.jsonc') then
                    client.config.settings = vim.tbl_deep_extend('force', client.config.settings, {
                        Lua = {
                            runtime = {
                                -- Tell the language server which version of Lua you're using
                                -- (most likely LuaJIT in the case of Neovim)
                                version = 'LuaJIT'
                            },
                            -- Make the server aware of Neovim runtime files
                            workspace = {
                                checkThirdParty = false,
                                library = vim.api.nvim_get_runtime_file("", true),
                            },
                        },
                    })

                    client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
                end
                return true
            end,
        })
    end,
}
