return {
    "kyazdani42/nvim-tree.lua",
    dependencies = {
        "kyazdani42/nvim-web-devicons"
    },
    keys = {
        "<leader><tab>",
    },
    config = function()
        local nvim_tree = require("nvim-tree")
        local api = require("nvim-tree.api")

        nvim_tree.setup({
            filters = {
                custom = {
                    "^.git$",
                },
            },
            actions = {
                open_file = {
                    window_picker = {
                        chars = "1234567890",
                    },
                },
            },
            git = {},
            renderer = {
                icons = {
                    glyphs = {
                        bookmark = "",
                        git = {
                            unstaged = "",
                            staged = "",
                            unmerged = "",
                            renamed = "",
                            untracked = "",
                            deleted = "",
                            ignored = "",
                        },
                    },
                },
            },
            view = {
                signcolumn = "no",
            },
        })

        vim.keymap.set("n", "<leader><tab>", api.tree.toggle)
    end,
}
