return {
    "nvim-lualine/lualine.nvim",
    config = function()
        local lualine = require("lualine")

        local Colors = {
            white        = "#ffffff",
            darkestgreen = "#005f00",
            brightgreen  = "#afdf00",
            darkestcyan  = "#005f5f",
            mediumcyan   = "#87dfff",
            darkestblue  = "#005f87",
            darkred      = "#870000",
            brightred    = "#df0000",
            brightorange = "#ff8700",
            gray0        = "#121212",
            gray1        = "#262626",
            gray2        = "#303030",
            gray3        = "#4e4e4e",
            gray4        = "#585858",
            gray5        = "#606060",
            gray6        = "#808080",
            gray7        = "#8a8a8a",
            gray8        = "#9e9e9e",
            gray9        = "#bcbcbc",
            gray10       = "#d0d0d0",
        }

        local powerline_muted = {
            normal = {
                a = { fg = Colors.darkestgreen, bg = Colors.brightgreen, gui = "bold" },
                b = { fg = Colors.white, bg = Colors.gray4 },
                c = { fg = Colors.gray8, bg = Colors.gray2 },
                x = { fg = Colors.gray8, bg = Colors.gray2 },
                y = { fg = Colors.gray9, bg = Colors.gray4 },
                z = { fg = Colors.gray5, bg = Colors.gray10 },
            },
            insert = {
                a = { fg = Colors.darkestcyan, bg = Colors.mediumcyan, gui = "bold" },
                z = { fg = Colors.gray5, bg = Colors.gray10 },
            },
            visual = {
                a = { fg = Colors.darkred, bg = Colors.brightorange, gui = "bold" },
                z = { fg = Colors.gray5, bg = Colors.gray10 },
            },
            replace = {
                a = { fg = Colors.white, bg = Colors.brightred, gui = "bold" },
                z = { fg = Colors.gray5, bg = Colors.gray10 },
            },
            inactive = {
                a = { fg = Colors.gray1, bg = Colors.gray5, gui = "bold" },
                b = { fg = Colors.gray1, bg = Colors.gray5 },
                c = { bg = Colors.gray1, fg = Colors.gray5 },
            },
        }

        lualine.setup({
            options = {
                theme = powerline_muted,
                icons_enabled = false,
                component_separators = { left = "", right = "|" },
                section_separators = { left = "", right = "" },
            },
            sections = {
                lualine_a = { "mode" },
                lualine_b = {
                    {
                        "filename",
                        path = 1,
                        symbols = {
                            readonly = " | RO",
                            modified = " | +",
                        },
                    },
                },
                lualine_c = {},
                lualine_x = { "fileformat", "encoding", "filetype" },
                lualine_y = { "progress" },
                lualine_z = { "location" }
            },
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = {
                    {
                        "filename",
                        path = 1,
                        symbols = {
                            readonly = "",
                            modified = " | +",
                        },
                    },
                },
                lualine_x = { "location" },
                lualine_y = {},
                lualine_z = {}
            },
            extensions = { "quickfix", "nvim-tree" },
        })
    end,
}
