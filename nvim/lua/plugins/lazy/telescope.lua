return {
    "nvim-telescope/telescope.nvim",
    dependencies = {
        "nvim-lua/plenary.nvim",
        "kyazdani42/nvim-web-devicons",
    },
    keys = {
        "<leader><cr>",
    },
    config = function()
        local telescope = require("telescope")

        telescope.setup({
            defaults = {
                layout_strategy = "horizontal",
                layout_config = {
                    horizontal = {
                        width = 0.9,
                        height = 0.9,
                        preview_width = { 0.7, max = 120 },
                        preview_cutoff = 80,
                    },
                },
                mappings = {
                    i = {
                        ["<C-u>"] = false,
                    },
                },
            }
        })

        local telescope_builtin = require("telescope.builtin")

        vim.keymap.set("n", "<leader><cr>", function()
            telescope_builtin.find_files({
                hidden = true,
            })
        end)
    end,
}
