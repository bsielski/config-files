vim.opt_local.cinoptions = '(0,:0,g0,l1,N-s,E-s,t0,u0,U0,Ws'
vim.opt_local.expandtab = false
vim.opt_local.shiftwidth = 8
vim.opt_local.tabstop = 8
vim.opt_local.textwidth = 100
