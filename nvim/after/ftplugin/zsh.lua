vim.opt_local.expandtab = false
vim.opt_local.shiftwidth = 8
vim.opt_local.tabstop = 8

if vim.fn.line('$') ~= 0 and vim.fn.getline(1) == '' then
    vim.cmd('startinsert')
end
