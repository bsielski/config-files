vim.opt_local.formatoptions:append('t')
vim.opt_local.textwidth = 78

-- Check if the first line of the commit message is empty
-- if it is, enter insert mode
if vim.fn.line('$') ~= 0 and vim.fn.getline(1) == '' then
    vim.cmd('startinsert')
end
