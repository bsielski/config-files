#!/hint/sh
if [ -z "${__ENVIRONMENT_D_LOADED:-}" ]; then
set -a
. /dev/fd/0 <<EOF
$(/usr/lib/systemd/user-environment-generators/30-systemd-environment-d-generator)
EOF
set +a
fi

prepend_path () {
	if [ -d "$1" ]; then
		case ":$PATH:" in
			*:"$1":*)
				;;
			*)
				PATH="$1${PATH:+:$PATH}"
		esac
	fi
}
prepend_path "$XDG_JUNKPILE/cargo/bin"
prepend_path "$XDG_BIN_HOME"
prepend_path "${XDG_CONFIG_HOME:-$HOME/.config}/bin"
unset -f prepend_path
